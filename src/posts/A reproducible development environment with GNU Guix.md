---
title: A reproducible development environment with GNU Guix
description: Why I started to use GNU Guix for development
---

## Context

I use the HDF5 C++ shared library compiled from source in a Python project with h5py. It used to work well but I recently had to compile some other C++ shared library (MuJoCo) to use with Python, which wasn't working. I somehow made it work by tweaking some environment variables but I later discovered that HDF5 wasn't compiling anymore...

## Guix to the rescue

Here's how I solved my HDF5 problem:
First I created a `manifest.scm` like below with the list of all the packages needed for the compilation>

```scheme
(specifications->manifest
 '("hdf5@1.10" "git" "pkg-config" "gcc-toolchain" "linux-libre-headers" "glibc@2.33"
   "bash" "python" "python-pip" "direnv" "coreutils" "which"))
```

Then I could run `guix shell -m manifest.scm --pure` to spawn an isolated shell without all my broken environment variables, and with the packages defined in the `manifest.scm` (so with HDF5 working).

## Conclusion

I tried using Docker but ran into problems with shared volumes, in the end using [Guix](https://guix.gnu.org/) was surprisingly easy and quite elegant: I could just spawn a new shell to get inside my working development environment.
