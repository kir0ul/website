// const dev = process.env.NODE_ENV === 'development';

// import adapter from '@sveltejs/adapter-auto';
import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter({
			// fallback: '200.html',
			// pages: 'public' // The directory to write prerendered pages to
		}),
		prerender: {
			default: true,
			enabled: true
		}
		// appDir: 'internal',
		// trailingSlash: 'always'
		// paths: {
		// 	base: dev ? '' : '/website',
		// },
	}
};

export default config;
